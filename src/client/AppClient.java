package client;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;

import server.InterfaceServerRemote;

public class AppClient 
{

	public static String process(int id)
	{
		String data= null;
		try 
		{
			Properties props= new Properties();
			//props.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
			props.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.wildfly.naming.client.WildFlyInitialContextFactory");
			InitialContext context= new InitialContext(props);
			
			String name_server = "EJB_APP_REMOTE";
			String name_bean = "ServiceConsult";
			String name_interface = InterfaceServerRemote.class.getName();
			
			String name = "ejb:/"+name_server+"/"+name_bean+"!"+name_interface;
			
			InterfaceServerRemote bean =(InterfaceServerRemote) context.lookup(name);
			data= bean.consult(id);
		}
		catch (Exception e) 
		{
			System.out.println("Errores: ");
			e.printStackTrace();
		}
			
		return data;
		
	}
	public static void main(String[] args) 
	{
		//String show = process(2);
		System.out.println(process(2));		
	}
	
}
